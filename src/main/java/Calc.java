public class Calc {

    static void Log(Object data) { System.out.println(data); }

    public static void main(String args[]) {
        System.out.println("Testing function manually");
        Log("Plus: " + CalcCore.plus(3f,2f));
        Log("Minus: " + CalcCore.minus(2f,5f));
        Log("Multiple: " + CalcCore.multiple(21f,2f));
        Log("Divide: " + CalcCore.divide(3f,17f));
    }
}
