
//Class for calculating float values
public class CalcCore {
    private static CalcCore ourInstance = new CalcCore();

    public static CalcCore getInstance() {
        return ourInstance;
    }

    //Prevent inheritance
    private CalcCore() {}

    static public float plus(Float a, float b) {
        return a+b;
    }

    static public float minus(Float a, float b) {
        return a-b;
    }

    static public float multiple(Float a, float b) {
        return a*b;
    }

    static public float divide(Float a, float b) {
        return a/b;
    }

}
