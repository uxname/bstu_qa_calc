import org.testng.Assert;

import static org.testng.Assert.*;

public class CalcCoreTest {

    /*PLUS*********************************************/
    //2+3=5
    @org.testng.annotations.Test
    public void testPlus1() throws Exception {
        Assert.assertEquals(5f, CalcCore.plus(2f,3f));
    }

    //MAX_VAL + MAX_VAL = INFINITY
    @org.testng.annotations.Test
    public void testPlus2() throws Exception {
        Assert.assertEquals(Float.POSITIVE_INFINITY, CalcCore.plus(Float.MAX_VALUE,Float.MAX_VALUE));
    }

    //0 + 0 = 0
    @org.testng.annotations.Test
    public void testPlus3() throws Exception {
        Assert.assertEquals(0f, CalcCore.plus(0f,0f));
    }

    //-5 + -5 = -10
    @org.testng.annotations.Test
    public void testPlus4() throws Exception {
        Assert.assertEquals(-10f, CalcCore.plus(-5f,-5f));
    }

    //9 + -9 = -0
    @org.testng.annotations.Test
    public void testPlus5() throws Exception {
        Assert.assertEquals(-0f, CalcCore.plus(9f,-9f)*-1);
    }

    /*MINUS********************************************/
    //5 - 3 = 2
    @org.testng.annotations.Test
    public void testMinus1() throws Exception {
        Assert.assertEquals(2f, CalcCore.minus(5f,3f));
    }

    //0 - 3 = -3
    @org.testng.annotations.Test
    public void testMinus2() throws Exception {
        Assert.assertEquals(-3, CalcCore.minus(0f,3f));
    }

    //0 - 0 = 0
    @org.testng.annotations.Test
    public void testMinus3() throws Exception {
        Assert.assertEquals(0f, CalcCore.minus(0f,0f));
    }

    //-5 - -5 = 10
    @org.testng.annotations.Test
    public void testMinus4() throws Exception {
        Assert.assertEquals(10f, CalcCore.minus(-5f,-5f));
    }

    //MIN_VAL - MAX_VAL = Float.MIN_VALUE
    @org.testng.annotations.Test
    public void testMinus5() throws Exception {
        Assert.assertEquals(-3.4028235E38f, CalcCore.minus(Float.MIN_VALUE,Float.MAX_VALUE));
    }


    /*MULTIPLE*****************************************/

    @org.testng.annotations.Test
    public void testMultiple1() throws Exception {
        Assert.assertEquals(100f, CalcCore.multiple(5f,20f));
    }

    @org.testng.annotations.Test
    public void testMultiple2() throws Exception {
        Assert.assertEquals(0f, CalcCore.multiple(5f,0f));
    }

    @org.testng.annotations.Test
    public void testMultiple3() throws Exception {
        Assert.assertEquals(300f, CalcCore.multiple(30f,10f));
    }

    @org.testng.annotations.Test
    public void testMultiple4() throws Exception {
        Assert.assertEquals(1f, CalcCore.multiple(2f,0.5f));
    }

    @org.testng.annotations.Test
    public void testMultiple5() throws Exception {
        Assert.assertEquals(-30f, CalcCore.multiple(-1f,30f));
    }


    /*DIVIDE*******************************************/


    @org.testng.annotations.Test
    public void testDivide1() throws Exception {
        Assert.assertEquals(4f, CalcCore.divide(20f,5f));
    }

    @org.testng.annotations.Test
    public void testDivide2() throws Exception {
        Assert.assertEquals(1.5f, CalcCore.divide(3f,2f));
    }

    @org.testng.annotations.Test
    public void testDivide3() throws Exception {
        Assert.assertEquals(0.1764706f, CalcCore.divide(3f,17f));
    }

    @org.testng.annotations.Test
    public void testDivide4() throws Exception {
        Assert.assertEquals(3f, CalcCore.divide(30f,10f));
    }

    @org.testng.annotations.Test
    public void testDivide5() throws Exception {
        Assert.assertEquals(1f, CalcCore.divide(2f,2f));
    }


}